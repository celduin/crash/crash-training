Training Data for Jetbots
=========================

This repo uses GIT LFS to store data to create models from.

Data in `lane_center/` is labelled with (x, y) coordinates of the manually
labelled "lane center".

Data in `end_to_end/` is labelled with the inverse reciprocal of the turning
radius (both normalised and in [cm^-1]) and the forward speed (again, both
normalised and in [cm/s]). For example:
```
img_141_R_-0.102~-0.196_V_0.268~13.942.jpg
    ^ unique id
          ^ inverse reciprocal (normalised)
                 ^ inverse reciprocal [1/cm]
                          ^ velocity normalised
                                ^ velocity [cm/s]
```
Note that for training, it probably makes sense to filter out the images where
the velocity was zero, as the steering command is less valid from a learning
perspective.
